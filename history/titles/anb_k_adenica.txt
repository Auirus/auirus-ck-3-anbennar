k_adenica = {
	1000.1.1 = { change_development_level = 8 }
}

d_adenica = {
	1000.1.1 = { change_development_level = 9 }
}

c_balgarton = {
	1000.1.1 = { change_development_level = 10 }
}

d_acengard = {
	1010.10.31 = {
		holder = 60014
	}
}

c_acengard = {
	1000.1.1 = { change_development_level = 9 }
	1010.10.31 = {
		holder = 60014
	}
}

c_carlanhal = {
	1000.1.1 = { change_development_level = 13 }
	1010.10.31 = {
		liege = d_acengard
	}
}

c_taranton = {
	1000.1.1 = { change_development_level = 11 }
	1010.10.31 = {
		liege = d_acengard
	}
}

d_valefort = {
	1016.7.8 = {
		holder = 76 #Martin Farran
		government = feudal_government
	}
}

c_valefort = {
	1000.1.1 = { change_development_level = 7 }
}

d_verteben = {
	1015.9.27 = {
		holder = 121 #Henric Verteben
	}
	1020.9.30 = {
		holder = 122 #Edith the Smith
	}
}

c_shieldrest = {
	1000.1.1 = { change_development_level = 7 }
}