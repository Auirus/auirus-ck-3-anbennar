k_adshaw = {
	1000.1.1 = { change_development_level = 7 }
	995.1.1 = {
		holder = 30016
	}
	1013.3.4 = {
		holder = 62 #Rylen Adshaw
	}
}

c_alencay = {
	1000.1.1 = { change_development_level = 8 }
	963.3.4 = {
		liege = k_adshaw
		holder = 183
	}
	997.12.07 = {
		liege = k_adshaw
		holder = 185
	}
	1020.02.28 = {
		liege = k_adshaw
		holder = 188
	}
}

c_frostwall = {
	1000.1.1 = { change_development_level = 6 }
	1000.1.1 = {
		liege = k_adshaw
		holder = 30001 # Betram Frostwall
	}
	1021.06.27 = {
		liege = k_adshaw
		holder = 30000 # Peter Goldeneyes
	}
}

c_everwharf = {
	1000.1.1 = { change_development_level = 7 }
	998.8.30 = {
		government = republic_government
		liege = k_adshaw
		holder = 30004 # Toste of Everwharf
	}
}

c_reachspier = {
	1000.1.1 = { change_development_level = 7 }
	1019.1.1 = {
		government = republic_government
		liege = k_adshaw
		holder = 30009 # Niklas of Reachspier
	}
}

d_adshaw = {
	1000.1.1 = { change_development_level = 9 }
	1013.3.4 = {
		holder = 62 #Rylen Adshaw
	}
}

c_rycastle = {
	1000.1.1 = { change_development_level = 9 }
	1013.3.4 = {
		holder = 62 #Rylen Adshaw
	}
}

c_north_greatwood = {
	1000.1.1 = { change_development_level = 6 }
	1013.3.4 = {
		holder = 62 #Rylen Adshaw
	}
}

c_dinesck = {
	1000.1.1 = { change_development_level = 9 }
	999.1.1 = {
		government = republic_government
		liege = k_adshaw
		holder = 30019 #Mathias of Dinesck
	}
}

c_adderwood = {
	1000.1.1 = { change_development_level = 6 }
	1013.3.4 = {
		holder = 62 #Rylen Adshaw
	}
}

c_envermarck = {
	1000.1.1 = { change_development_level = 8 }
	999.1.1 = {
		government = republic_government
		liege = k_adshaw
		holder = 30018 # Helena of Envermark
	}
}

c_coldmarket = {
	1000.1.1 = { change_development_level = 7 }
	999.1.1 = {
		government = republic_government
		liege = k_adshaw
		holder = 30010 # Valdemar of Coldmarket
	}
}

d_celmaldor = {
	1002.3.2 = {
		holder = 61 #Maldorian the Navigator
		government = republic_government
	}
}

c_celmaldor = {
	1000.1.1 = { change_development_level = 8 }
	1002.3.2 = {
		holder = 61 #Maldorian the Navigator
	}
}

d_serpentgard = {
	990.2.9 = {
		holder = 63 #Rikkard Serpentsgard
	}
}

c_serpentback = {
	1000.1.1 = { change_development_level = 8 }
}

c_mawdock = {
	990.2.9 = {
		holder = 63 #Rikkard Serpentsgard
	}
}

c_eyegard = {
	990.2.9 = {
		holder = 63 #Rikkard Serpentsgard
	}
}

d_bayvic = {
	1000.1.1 = { change_development_level = 11 }
	1017.5.4 = {
		holder = 64 #Mason Cobbler
		government = republic_government
	}
}

c_bayvic = {
	1000.1.1 = { change_development_level = 11 }
	1017.5.4 = {
		holder = 64 #Mason Cobbler
	}
}