d_nihruqalu= {
	1020.6.4= {
		holder = bulwari0126
	}
}

c_daqalm = {
	1000.1.1 = { change_development_level = 1 }
	960.4.6 = {
		holder = yazkur_0002
	}
	990.9.6 = {
		holder = yazkur_0003
	}
}

c_ardulib = {
	1000.1.1 = { change_development_level = 1 }

	989.2.4 = {
		holder = ayarzil_0001
	}
	1008.4.4 = {
		holder = ayarzil_0002
	}
}

c_shimlardu = {
	1000.1.1 = { change_development_level = 1 }

	989.2.4 = {
		holder = ayarzil_0001
	}
	1008.4.4 = {
		holder = ayarzil_0002
	}
}

c_edesukeru = {
	1000.1.1 = { change_development_level = 1 }

	973.3.8 = {
		holder = attalu_0002
	}
	1006.2.8 = {
		holder = attalu_0003
	}
}

c_kesud = {
	1000.1.1 = { change_development_level = 1 }

	973.3.8 = {
		holder = attalu_0002
	}
	1006.2.8 = {
		holder = attalu_0003
	}
}

c_subkaliss = {
	1000.1.1 = { change_development_level = 1 }

	973.3.8 = {
		holder = attalu_0002
	}
	1006.2.8 = {
		holder = attalu_0003
	}
}

c_sahnuses = {
	1000.1.1 = { change_development_level = 1 }

	973.3.8 = {
		holder = attalu_0002
	}
	1006.2.8 = {
		holder = attalu_0003
	}
}
c_saranza = {
	1000.1.1 = { change_development_level = 1 }

	1019.7.2 = {
		holder = surubaz_0001
	}
}

c_piruharran = {
	1000.1.1 = { change_development_level = 1 }

	1000.1.2 = {
		holder = selabis_0001
	}
}

c_edenlil = {
	1000.1.1 = { change_development_level = 1 }

	1000.1.2 = {
		holder = betikalbu_0001
	}
}