#k_merewood
##d_merescker
###c_moreced
785 = {		#Moreced

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2293 = {

    # Misc
    holding = church_holding

    # History

}
2294 = {

    # Misc
    holding = city_holding

    # History

}

###c_turnmarket
787 = {		#Turnmarket

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2295 = {

    # Misc
    holding = none

    # History

}
2296 = {

    # Misc
    holding = city_holding

    # History

}

###c_esckerport
2297 = {		#Esckerport

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
789 = {

    # Misc
    holding = city_holding

    # History

}
2298 = {

    # Misc
    holding = none

    # History

}

###c_aesawic
786 = {		#Aesawic

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2299 = {

    # Misc
    holding = church_holding

    # History

}
2300 = {

    # Misc
    holding = none

    # History

}

###c_craghyl
784 = {		#Craghyl

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2301 = {

    # Misc
    holding = city_holding

    # History

}
2302 = {

    # Misc
    holding = church_holding

    # History

}

##d_nortmerewood
###c_nortmerewood
788 = {		#Nortmerewood

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2290 = {

    # Misc
    holding = none

    # History

}

###c_woudbet
790 = {		#Woudbet

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2291 = {

    # Misc
    holding = city_holding

    # History

}
2292 = {

    # Misc
    holding = none

    # History

}

###c_acenaire
783 = {		#Acenaire

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2288 = {

    # Misc
    holding = city_holding

    # History

}
2289 = {

    # Misc
    holding = none

    # History

}

###c_aldaine
766 = {		#Aldaine

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2285 = {

    # Misc
    holding = city_holding

    # History

}
2286 = {

    # Misc
    holding = church_holding

    # History

}
2287 = {

    # Misc
    holding = none

    # History

}

##d_oudmerewood
###c_oudmerewood
794 = {		#Oudmerewood

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2303 = {

    # Misc
    holding = church_holding

    # History

}
2304 = {

    # Misc
    holding = none

    # History

}

###c_acenthan
795 = {		#Acenthan

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2305 = {

    # Misc
    holding = city_holding

    # History

}
2306 = {

    # Misc
    holding = church_holding

    # History

}

###c_uanced
796 = {		#Uanced

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2307 = {

    # Misc
    holding = church_holding

    # History

}

###c_leighalen
793 = {		#Leighalen

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2308 = {

    # Misc
    holding = none

    # History

}
