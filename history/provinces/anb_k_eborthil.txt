#k_eborthil
##d_eborthil
###c_eborthil
380 = {		#Eborthil

    # Misc
    culture = tefori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1622 = {

    # Misc
    holding = castle_holding

    # History

}
1623 = {

    # Misc
    holding = church_holding

    # History

}
1624 = {

    # Misc
    holding = none

    # History

}
1625 = {

    # Misc
    holding = city_holding

    # History

}

###c_port_jurith
369 = {		#Port Jurith

    # Misc
    culture = tefori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1626 = {

    # Misc
    holding = city_holding

    # History

}
1639 = {

    # Misc
    holding = none

    # History

}

##d_tefkora
###c_new_tefkora
1631 = {		#New Tefkora

    # Misc
    culture = tefori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
36 = {

    # Misc
    holding = none

    # History

}
1630 = {

    # Misc
    holding = none

    # History

}
1632 = {

    # Misc
    holding = church_holding

    # History

}

###c_toref_citadel
35 = {		#Toref Citadel

    # Misc
    culture = tefori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1629 = {

    # Misc
    holding = city_holding

    # History

}
1627 = {

    # Misc
    holding = church_holding

    # History

}

###c_stonegaze
379 = {		#Stonegaze

    # Misc
    culture = tefori
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1628 = {

    # Misc
    holding = city_holding

    # History

}
