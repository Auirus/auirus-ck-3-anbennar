#k_reveria
##d_reaver_coast
###c_reavers_landing
138 = {		#Reavers Landing

    # Misc
    culture = reverian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
1393 = {

    # Misc
    holding = church_holding

    # History

}
1363 = {

    # Misc
    holding = city_holding

    # History

}
1359 = {

    # Misc
    holding = none

    # History

}

###c_stormpoint
140 = {		#Stormpoint

    # Misc
    culture = reverian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
1390 = {

    # Misc
    holding = none

    # History

}
1368 = {

    # Misc
    holding = city_holding

    # History

}

###c_baynoms
122 = {		#Baynoms

    # Misc
    culture = reverian
    religion = castanorian_pantheon
	holding = city_holding

    # History
}
1391 = {

    # Misc
    holding = church_holding

    # History

}

##d_gnomish_pass
###c_manerd
144 = {		#Manerd

    # Misc
    culture = reverian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
1394 = {

    # Misc
    holding = church_holding

    # History

}
1395 = {

    # Misc
    holding = city_holding

    # History

}

###c_olmaddit
145 = {

    # Misc
    culture = reverian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
1396 = {

    # Misc
    holding = city_holding

    # History

}

###c_manyburrows
148 = {		#Manyburrows

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1397 = {

    # Misc
    holding = none

    # History

}
1405 = {

    # Misc
    holding = church_holding

    # History

}
1406 = {

    # Misc
    holding = none

    # History

}

###c_royvibobb
167 = {		#Royvibobb

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1401 = {

    # Misc
    holding = city_holding

    # History

}
1403 = {

    # Misc
    holding = none

    # History

}
1404 = {

    # Misc
    holding = none

    # History

}
