#k_gawed
##d_gawed
###c_gaweton
221 = {		#Gaweton

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1972 = {

    # Misc
    holding = church_holding

    # History

}
1973 = {

    # Misc
    holding = city_holding

    # History

}
1974 = {

    # Misc
    holding = none

    # History

}

###c_vanbury
222 = {		#Vanbury

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = vanbury_mines_01
		special_building = vanbury_mines_01
	}
}
1981 = {

    # Misc
    holding = none

    # History

}
1982 = {

    # Misc
    holding = church_holding

    # History

}
1983 = {

    # Misc
    holding = city_holding

    # History

}

###c_drakesford
223 = {		#Drakesford

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1986 = {

    # Misc
    holding = church_holding

    # History

}
1987 = {

    # Misc
    holding = none

    # History

}
1988 = {

    # Misc
    holding = none

    # History

}

###c_norleigh
341 = {		#Norleigh

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1984 = {

    # Misc
    holding = city_holding

    # History

}
1985 = {

    # Misc
    holding = none

    # History

}

###c_somberwold
346 = {		#Somberwold

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1978 = {

    # Misc
    holding = city_holding

    # History

}
1979 = {

    # Misc
    holding = none

    # History

}

##d_westmounts
###c_talonmount
217 = {		#Talonmount

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1967 = {

    # Misc
    holding = city_holding

    # History

}
1968 = {

    # Misc
    holding = none

    # History

}

###c_wightsgate
340 = {		#Wightsgate

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1969 = {

    # Misc
    holding = city_holding

    # History

}
1970 = {

    # Misc
    holding = none

    # History

}

###c_henhurst
1965 = {	#Henhurst Hall

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1966 = {

    # Misc
    holding = church_holding

    # History

}

###c_westfield
218 = {		#Westfield

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1971 = {

    # Misc
    holding = none

    # History

}

##d_ginnfield
###c_ginnfield
220 = {		#Ginnfield

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1962 = {

    # Misc
    holding = none

    # History

}
1963 = {

    # Misc
    holding = church_holding

    # History

}
1964 = {

    # Misc
    holding = city_holding

    # History

}

###c_alenath
337 = {		#Alenath

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1959 = {

    # Misc
    holding = city_holding

    # History

}

###c_elwick_upon_alen
339 = {		#Elwick-upon-Alen

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1960 = {

    # Misc
    holding = church_holding

    # History

}
1961 = {

    # Misc
    holding = none

    # History

}

##d_vertesk
###c_vertesk
216 = {		#Vertesk

    # Misc
    culture = vertesker
    religion = cult_of_ara
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_vertesk_01
		special_building = castanorian_citadel_bal_vertesk_01
	}
}
1948 = {

    # Misc
    holding = none

    # History

}
1949 = {

    # Misc
    holding = city_holding

    # History

}
1950 = {

    # Misc
    holding = church_holding

    # History

}
1958 = {

    # Misc
    holding = city_holding

    # History

}

###c_tenbury
219 = {		#Tenbury

    # Misc
    culture = vertesker
    religion = cult_of_ara
	holding = castle_holding

    # History
}
1951 = {

    # Misc
    holding = city_holding

    # History

}
1952 = {

    # Misc
    holding = church_holding

    # History

}

###c_greenley
235 = {		#Greenley

    # Misc
    culture = vertesker
    religion = cult_of_ara
	holding = castle_holding

    # History
}
1953 = {

    # Misc
    holding = church_holding

    # History

}
1954 = {

    # Misc
    holding = none

    # History

}
1423 = {

    # Misc
    holding = none

    # History

}

###c_highharbour
251 = {		#Highharbour

    # Misc
    culture = vertesker
    religion = cult_of_ara
	holding = castle_holding

    # History
}
1955 = {

    # Misc
    holding = city_holding

    # History

}
1956 = {

    # Misc
    holding = church_holding

    # History

}
1957 = {

    # Misc
    holding = none

    # History

}

##d_alenic_expanse
###c_alenfield
239 = {		#Alenfield

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
2004 = {

    # Misc
    holding = church_holding

    # History

}

###c_swinthorpe
2002 = {

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}

2003 = {

    # Misc
    holding = none

    # History

}

###c_baldfather
237 = {		#Baldfather

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1998 = {

    # Misc
    holding = city_holding

    # History

}
1999 = {

    # Misc
    holding = church_holding

    # History

}
2000 = {

    # Misc
    holding = none

    # History

}
2001 = {

    # Misc
    holding = none

    # History

}

###c_exeham
1991 = {

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1992 = {

    # Misc
    holding = city_holding

    # History

}

###c_oxington
1993 = {

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1994 = {

    # Misc
    holding = none

    # History

}

###c_humbercroft
224 = {		#Humbercroft

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1995 = {

    # Misc
    holding = church_holding

    # History

}
1996 = {

    # Misc
    holding = none

    # History

}
1997 = {

    # Misc
    holding = none

    # History

}

###c_gerwick
243 = {		#Gerwick

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
2005 = {

    # Misc
    holding = church_holding

    # History

}
2006 = {

    # Misc
    holding = city_holding

    # History

}
2007 = {

    # Misc
    holding = none

    # History

}

###c_morban
338 = {		#Morban

    # Misc
    culture = morbanite
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2008 = {

    # Misc
    holding = city_holding

    # History

}
2009 = {

    # Misc
    holding = none

    # History

}

##d_greatmarch
###c_greatwoods
240 = {		#Greatwoods

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
2017 = {

    # Misc
    holding = none

    # History

}
2018 = {

    # Misc
    holding = none

    # History

}
2019 = {

    # Misc
    holding = none

    # History

}
2020 = {

    # Misc
    holding = city_holding

    # History

}

###c_northal
2031 = {

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
2032 = {

    # Misc
    holding = none

    # History

}

###c_alenvord
225 = {		#Alenvord

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2021 = {

    # Misc
    holding = church_holding

    # History

}
2022 = {

    # Misc
    holding = none

    # History

}
2023 = {

    # Misc
    holding = none

    # History

}
2024 = {

    # Misc
    holding = none

    # History

}

###c_legions_clearing
226 = {		#Legion's Clearing

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2025 = {

    # Misc
    holding = city_holding

    # History

}
# 2026 = { # undefined province

    # # Misc
    # holding = none

    # # History

# }
2027 = {

    # Misc
    holding = none

    # History

}

##d_balvord
###c_gardfort
228 = {		#Gardfort

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2034 = {

    # Misc
    holding = none

    # History

}

###c_wolfden
241 = {		#Wolfden

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2028 = {

    # Misc
    holding = church_holding

    # History

}
2029 = {

    # Misc
    holding = none

    # History

}
2030 = {

    # Misc
    holding = none

    # History

}

###c_aldtempel
227 = {		#Aldtempel

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2033 = {

    # Misc
    holding = none

    # History

}

##d_oudescker
###c_oudescker
344 = {		#Oudescker

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2014 = {

    # Misc
    holding = city_holding

    # History

}
2015 = {

    # Misc
    holding = none

    # History

}
2016 = {

    # Misc
    holding = none

    # History

}

###c_jonsway
343 = {		#Jonsway

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2010 = {

    # Misc
    holding = none

    # History

}

###c_mossford
242 = {		#Mossford

    # Misc
    culture = old_alenic
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
2011 = {

    # Misc
    holding = church_holding

    # History

}
2012 = {

    # Misc
    holding = none

    # History

}
2013 = {

    # Misc
    holding = none

    # History

}

##d_arbaran
###c_arbaran
349 = {		#Arbaran

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2071 = {

    # Misc
    holding = church_holding

    # History

}
2072 = {

    # Misc
    holding = none

    # History

}
2073 = {

    # Misc
    holding = city_holding

    # History

}

###c_arca_dhanaenn
244 = {		#Arca Dhanaenn

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2066 = {

    # Misc
    holding = church_holding

    # History

}
2067 = {

    # Misc
    holding = city_holding

    # History

}
2068 = {

    # Misc
    holding = none

    # History

}
2069 = {

    # Misc
    holding = none

    # History

}
2070 = {

    # Misc
    holding = none

    # History

}

###c_arca_pirvar
252 = {		#Arca Pirvar

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2074 = {

    # Misc
    holding = none

    # History

}
2075 = {

    # Misc
    holding = city_holding

    # History

}
2076 = {

    # Misc
    holding = none

    # History

}

###c_northwatch
238 = {		#Northwatch

    # Misc
    culture = morbanite
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2077 = {

    # Misc
    holding = none

    # History

}

###c_freecestir
245 = {		#Freecestir

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2061 = {

    # Misc
    holding = city_holding

    # History

}
2062 = {

    # Misc
    holding = church_holding

    # History

}
2063 = {

    # Misc
    holding = none

    # History

}

###c_elvenaire
256 = {		#Elvenaire

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2058 = {

    # Misc
    holding = none

    # History

}
2059 = {

    # Misc
    holding = church_holding

    # History

}
2060 = {

    # Misc
    holding = none

    # History

}

###c_fort_vigil
246 = {		#Fort Vigil

    # Misc
    culture = morbanite
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2055 = {

    # Misc
    holding = city_holding

    # History

}
2056 = {

    # Misc
    holding = none

    # History

}
2057 = {

    # Misc
    holding = none

    # History

}

###c_mirewatch
247 = {		#Mirewatch

    # Misc
    culture = morbanite
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2064 = {

    # Misc
    holding = church_holding

    # History

}
2065 = {

    # Misc
    holding = none

    # History

}
