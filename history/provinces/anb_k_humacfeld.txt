#k_humacfeld
##d_ionntras
###c_ionntras
827 = {		#Ionntras

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2390 = {

    # Misc
    holding = church_holding

    # History

}
2391 = {

    # Misc
    holding = city_holding

    # History

}
2392 = {

    # Misc
    holding = none

    # History

}
2393 = {

    # Misc
    holding = none

    # History

}

###c_forkwic
856 = {		#Forkwic

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2398 = {

    # Misc
    holding = city_holding

    # History

}
2399 = {

    # Misc
    holding = church_holding

    # History

}
2400 = {

    # Misc
    holding = none

    # History

}

###c_grannvale
857 = {		#Grannvale

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2401 = {

    # Misc
    holding = city_holding

    # History

}
2402 = {

    # Misc
    holding = none

    # History

}

###c_wystanway
826 = {		#Wystanway

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2394 = {

    # Misc
    holding = none

    # History

}

###c_humacs_rest
824 = { #Humac's Rest

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2395 = {

    # Misc
    holding = church_holding

    # History

}
2396 = {

    # Misc
    holding = none

    # History

}
2397 = {

    # Misc
    holding = none

    # History

}

##d_wallor
###c_silverdocks
829 = {

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2404 = {

    # Misc
    holding = city_holding

    # History

}
2405 = {

    # Misc
    holding = church_holding

    # History

}

###c_southroad
2406 = {	#Southroad

    # Misc
    culture = castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2407 = {

    # Misc
    holding = city_holding

    # History

}

###c_treomar
2409 = {	#Treomar

    # Misc
    culture = castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2408 = {

    # Misc
    holding = church_holding

    # History

}

##d_themin
###c_theminath
820 = {		#Theminath

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2375 = {

    # Misc
    holding = church_holding

    # History

}
2376 = {

    # Misc
    holding = none

    # History

}

###c_haresleigh
822 = {		#Haresleigh

	# Misc
	culture = marcher
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2382 = {

    # Misc
    holding = city_holding

    # History

}
2383 = {

    # Misc
    holding = none

    # History

}

##d_burnoll
###c_burnoll
823 = {		#Burnoll

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2387 = {

    # Misc
    holding = church_holding

    # History

}
2388 = {

    # Misc
    holding = city_holding

    # History

}
2389 = {

    # Misc
    holding = none

    # History

}

###c_camircost
821 = {		#Camircost

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2377 = {

    # Misc
    holding = church_holding

    # History

}
2378 = {

    # Misc
    holding = none

    # History

}
2379 = {

    # Misc
    holding = none

    # History

}
2380 = {

    # Misc
    holding = none

    # History

}
2381 = {

    # Misc
    holding = city_holding

    # History

}

###c_smallmere
825 = {		#Smallmere

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2384 = {

    # Misc
    holding = city_holding

    # History

}
2385 = {

    # Misc
    holding = none

    # History

}
2386 = {

    # Misc
    holding = none

    # History

}
