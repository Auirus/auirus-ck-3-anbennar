﻿namespace = anb_dominate_victim

# You being are dominated! #
anb_dominate_victim.1 = {
	type = character_event
	title = anb_dominate_victim.1.t
	desc = anb_dominate_victim.1.d

	theme = witchcraft

	right_portrait = {
		character = this
		animation = schock
	}
	
	option = {
		name = anb_dominate_victim.1.a
	}
}

# They freed themselves! #
anb_dominate_victim.2 = {
	type = character_event
	title = anb_dominate_victim.2.t
	desc = anb_dominate_victim.2.d

	theme = witchcraft

	left_portrait = {
		character = this
		animation = disbelief
	}

	right_portrait = {
		character = freed_person
		animation = ecstasy
	}
	
	option = {
		name = anb_dominate_victim.2.a
	}
}

# Somebody is already dominating them! #
anb_dominate_victim.3 = {
	type = character_event
	title = anb_dominate_victim.3.t
	desc = anb_dominate_victim.3.d

	theme = witchcraft

	left_portrait = {
		character = this
		animation = disbelief
	}

	right_portrait = {
		character = scope:target
		animation = idle
	}
	
	immediate = {
		scope:scheme = {
			add_scheme_modifier = { type = dominate_opposing_presence }
		}
	}
	
	option = {
		name = anb_dominate_victim.3.a
	}
}

# You freed yourself! #
anb_dominate_victim.4 = {
	type = character_event
	title = anb_dominate_victim.4.t
	desc = anb_dominate_victim.4.d

	theme = friendly

	left_portrait = {
		character = this
		animation = ecstasy
	}
	
	option = {
		name = anb_dominate_victim.4.a
	}
}

# The voice goes quiet.. (controller died) #
anb_dominate_victim.5 = {
	type = character_event
	title = anb_dominate_victim.5.t
	desc = anb_dominate_victim.5.d

	theme = friendly

	left_portrait = {
		character = this
		animation = ecstasy
	}
	
	option = {
		name = anb_dominate_victim.5.a
	}
}