﻿#Anbennar - cleared these

#Anbennar
special_succession_elven_effect = {
	if = {
		limit = { highest_held_title_tier = tier_empire }
		every_held_title = {
			limit = {
				tier = tier_empire
				NOR = {
					has_title_law_flag = advanced_succession_law
					has_title_law_flag = elective_succession_law
				}
			}
			add_title_law = elven_elective_succession_law
		}
		every_vassal = {
			limit = { is_elvish_culture = yes }
			add_opinion = {
				modifier = implemented_traditional_succession_law_opinion
				years = 20
				target = root
			}
			custom = major_decisions.3100.tt_opinion_elven
		}
	}
	else = {
		every_held_title = {
			limit = {
				tier = tier_kingdom
				NOR = {
					has_title_law_flag = advanced_succession_law
					has_title_law_flag = elective_succession_law
				}
			}
			add_title_law = elven_elective_succession_law
		}
		every_vassal = {
			limit = { is_elvish_culture = yes }
			add_opinion = {
				modifier = implemented_traditional_succession_law_opinion
				years = 20
				target = root
			}
			custom = major_decisions.3100.tt_opinion_elven
		}
	}
}

form_castellyr_decision_effects = {
	#Prestige
	add_prestige = massive_prestige_gain

	#Laws
	if = {
		limit = { has_realm_law = crown_authority_0 }
		add_realm_law_skip_effects = crown_authority_2
	}
	else_if = {
		limit = {
			OR = {
				has_realm_law = crown_authority_1
				has_realm_law = crown_authority_2
			}
		}
		add_realm_law_skip_effects = crown_authority_3
	}
	
	#Form Castellyr
	create_title_and_vassal_change = {
		type = created
		save_scope_as = change
		add_claim_on_loss = no
	}
	title:k_castellyr = {
		change_title_holder = {
			holder = root
			change = scope:change
		}
		copy_title_history = scope:castellyr_former.primary_title
	}
	resolve_title_and_vassal_change = scope:change
	hidden_effect = { set_primary_title_to = title:k_castellyr }

	#Handle other Kingdom titles
	title:k_cast = { add_to_list = inner_castanor_kingdoms }
	title:k_anor = { add_to_list = inner_castanor_kingdoms }
	title:k_castonath = { add_to_list = inner_castanor_kingdoms }

	custom_tooltip = form_castellyr_decision_titles_tt
	hidden_effect = {
		every_in_list = {
			list = inner_castanor_kingdoms
			limit = {
				NOT = { this = scope:castellyr_former.primary_title }
				scope:castellyr_former = { completely_controls = prev }
			}

			#De jure shift all held spanish titles into your primary title
			every_in_de_jure_hierarchy = {
				limit = { tier = tier_duchy }
				set_de_jure_liege_title = scope:castellyr_former.primary_title
			}

			#Destroys them all!
			scope:castellyr_former = { destroy_title = prev }
		}
	}
	
	#Change to Castellyrian culture
	culture:castellyrian = {
		get_all_innovations_from = root.culture
	}
	
	#Convert your, and your whole family's, culture
	custom_tooltip = form_castellyr_decision_culture_change
	hidden_effect = {
		set_culture = culture:castellyrian
		every_spouse = {
			limit = {
				is_landed = no
				OR = {
					has_trait = race_human
					has_trait = race_half_elf
				}
			}
			add_to_list = characters_to_convert
		}
		every_close_family_member = {
			limit = {
				is_landed = no
				NOT = { is_spouse_of = root }
				OR = {
					has_trait = race_human
					has_trait = race_half_elf
				}
			}
			custom = all_unlanded_family_members
			add_to_list = characters_to_convert
		}
		every_courtier = {
			limit = {
				OR = {
					has_trait = race_human
					has_trait = race_half_elf
				}
			}
			add_to_list = characters_to_convert
		}

		#Convert appropriate vassals, and their family
		every_vassal_or_below = {
			limit = {
				is_ai = yes
				OR = {
					has_trait = race_human
					has_trait = race_half_elf
				}
				primary_title = {
					OR = {
						de_jure_liege = title:k_castellyr
						de_jure_liege.de_jure_liege = title:k_castellyr
						de_jure_liege.de_jure_liege.de_jure_liege = title:k_castellyr
					}
				}
			}
			add_to_list = characters_to_convert
			every_courtier = {
				limit = {
					OR = {
						has_trait = race_human
						has_trait = race_half_elf
					}
				}
				add_to_list = characters_to_convert
			}
		}
		
		every_in_list = {
			list = characters_to_convert
			set_culture = culture:castellyrian
		}
	}

	#Flip Counties
	every_county_in_region = {
		region = custom_inner_castanor
		custom = form_castellyr_decision_culture_counties_custom
		limit = {
			OR = {
				culture = culture:castanorian
				culture = culture:black_castanorian
			}
		}
		set_county_culture = culture:castellyrian
	}
}


form_the_reach_effect = {
	#Prestige
	add_prestige = massive_prestige_gain
	
	#Innovations
	culture = {
		add_random_innovation = culture_group_military
		add_random_innovation = culture_group_civic
	}

	#Laws
	if = {
		limit = { has_realm_law = crown_authority_0 }
		add_realm_law_skip_effects = crown_authority_2
	}
	else_if = {
		limit = {
			OR = {
				has_realm_law = crown_authority_1
				has_realm_law = crown_authority_2
			}
		}
		add_realm_law_skip_effects = crown_authority_3
	}

	#Form The Reach
	create_title_and_vassal_change = {
		type = created
		save_scope_as = change
		add_claim_on_loss = no
	}
	title:k_the_reach = {
		change_title_holder = {
			holder = root
			change = scope:change
		}
		hidden_effect = { copy_title_history = scope:the_reach_former.primary_title }
	}
	resolve_title_and_vassal_change = scope:change
	hidden_effect = { set_primary_title_to = title:k_the_reach }

	#Handle other Kingdom titles
	title:k_adshaw = { add_to_list = the_reach_kingdoms }
	title:k_vrorenmarch = { add_to_list = the_reach_kingdoms }

	custom_tooltip = unite_the_reach_decision_titles_tt
	hidden_effect = {
		every_in_list = {
			list = the_reach_kingdoms
			limit = {
				NOT = { this = scope:the_reach_former.primary_title }
				scope:the_reach_former = { completely_controls = prev }
			}

			#De jure shift all held spanish titles into your primary title
			every_in_de_jure_hierarchy = {
				limit = { tier = tier_duchy }
				set_de_jure_liege_title = scope:the_reach_former.primary_title
			}

			#Destroys them all!
			scope:the_reach_former = { destroy_title = prev }
		}
	}
}

move_all_valid_reach_kingdoms_effect = {
	custom_tooltip = bring_the_reach_into_the_fold_decision_effect_tt

	move_reach_title_into_gerudia_effect = { TITLE = title:k_adshaw }
	move_reach_title_into_gerudia_effect = { TITLE = title:k_vrorenmarch }
}

move_reach_title_into_gerudia_effect = {
	if = {
		limit = {
			can_move_title_into_gerudia_trigger = { TITLE = $TITLE$ }
		}
		$TITLE$ = {
			set_de_jure_liege_title = title:e_gerudia
			set_variable = kingdom_moved_into_gerudia_empire
		}
		add_prestige = medium_prestige_gain
	}
}