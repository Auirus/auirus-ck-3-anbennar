﻿convert_to_elvenized_culture_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	desc = convert_to_elvenized_culture_decision_desc
	selection_tooltip = convert_to_elvenized_culture_decision_tooltip

	is_shown = {
		is_landed = yes
		primary_title.tier > tier_barony
		culture = {
			culture_can_be_elvenized_trigger = yes
		}
		NAND = {
			is_ai = yes
			OR = {
				has_character_flag = converted_culture_this_lifetime
				has_character_flag = converted_culture_this_lifetime_ai
			}
		}
	}
	
	is_valid_showing_failures_only = {
		is_available_adult = yes
		is_at_war = no
		custom_description = {
			text = can_only_change_culture_once
			NOT = { has_character_flag = converted_culture_this_lifetime }
		}
	}

	is_valid = {
		trigger_if = {
			limit = { is_independent_ruler = yes }
			prestige_level >= 3
			OR = {
				has_trait = race_elf
				has_trait = race_half_elf
			}
		}
		trigger_if = {
			limit = { is_independent_ruler = no }
			OR = {
				custom_description = {
					text = ruler_elvenized_tooltip
					top_liege.culture = {
						has_innovation = innovation_elvenization
					}
				}
				top_liege = {
					has_trait = race_elf
				}
			}
			OR = {
				trigger_if = {
					limit = { has_trait = race_elf }
					has_trait = race_elf
				}
				has_trait = race_half_elf
				trigger_if = {
					limit = { exists = primary_spouse }
					primary_spouse = {
						OR = {
							has_trait = race_elf
							has_trait = race_half_elf
						}
					}
				}
				any_councillor = {
					is_powerful_vassal = yes
					OR = {
						has_trait = race_elf
						has_trait = race_half_elf
					}
				}
				custom_description = {
					text = capital_county_elvenized_tooltip
					capital_province.culture = {
						has_innovation = innovation_elvenization
					}
				}
			}
		}
	}

	cost = {
		prestige = convert_to_local_culture_base_cost
	}

	effect = {
		convert_to_elvenized_culture_effect = yes
	}
	
	ai_check_interval = 60

	ai_potential = {
		is_landed = yes
		culture = {
			culture_can_be_elvenized_trigger = yes
		}
	}

	ai_will_do = {
		base = 3
		
		modifier = { # If your liege is the same culture as your capital, better get on with it! This should combat scattered wrong-culture Counts and such.
			add = 77
			capital_province.culture = liege.culture
		}
		
		modifier = { # Motivate vassals to assimilate if they are different culture from their liege. If they can't curry their liege's favor, better get the peasants on your side at least...
			add = 27
			NOR = {
				culture = capital_province.culture
				culture = liege.culture
			}
		}
		
		modifier = { # If your liege is your parent, do not convert
			factor = 0
			exists = liege
			culture = liege.culture
			OR = {
				liege = root.mother
				liege = root.father
			}
		}
		
		# Don't go back on a previous conversion
		modifier = {
			factor = 0
			has_character_flag = converted_culture_this_lifetime
		}
		
		# Human purist don't convert
		modifier = {
			factor = 0
			has_trait = human_purist
		}
	}
}

