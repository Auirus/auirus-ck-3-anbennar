﻿bladestewards_can_exist_trigger = {
	NOR = {
		# Can't currently be created.
		exists = global_var:bladestewards_title
	}
}

valid_county_for_bladestewards_trigger = {
	tier = tier_county
	custom_description = {
		text = is_in_blademarches_kingdom_tooltip
		title_province = { kingdom = title:k_blademarches }
	}
	any_county_province = {
		custom_description = {
			text = empty_castle_holding
			OR = {
				has_holding = no
				has_holding_type = city_holding
			}
		}
	}
	bladestewards_exist_trigger = no
}

bladestewards_exist_trigger = {
	custom_description = {
		text = order_of_bladestewards_established_tooltip
		title:c_stewards_hold = {
			has_county_modifier = bladestewards_present
		}
	}
}