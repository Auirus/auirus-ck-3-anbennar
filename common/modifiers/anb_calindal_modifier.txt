﻿wielded_calindal = {
	icon = martial_positive
	prowess = 1 # Cause they are known to weild a magical blade
}

blinded_by_calindal = {
	icon = martial_negative
	diplomacy = -1 # You also get blinded
}

refused_calindal = {
	icon = diplomacy_negative
}

bladestewards_present = {
	icon = prowess_positive
	tax_mult = -0.10
	levy_size = -0.10
	development_growth_factor = 0.2
	development_growth = 0.1
}

bladesteward = {
	icon = prowess_positive
	monthly_prestige = 0.3
	monthly_dynasty_prestige = 0.04
	prowess = 11
	martial_per_prestige_level = 1
	learning_per_prestige_level = -1
	knight_effectiveness_mult = 0.10
}

untested_bladeking = {
	icon = diplomacy_negative
	direct_vassal_opinion = -5
}