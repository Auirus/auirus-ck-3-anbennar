﻿##########################
#   TAYBEAN MADE THIS!   #
#         TELLUM         #
##########################

# I want to run this once, and have it as an attribute of the character
# calculate how many human years is equivalent to race years
calculate_age_ratio = {
	value = 0

	if = {
		limit = {
			race_has_400_year_lifespan = yes	#elves
		}
		add = 400
	}
	else_if = {
		limit = {
			race_has_250_year_lifespan = yes	#gnomes
		}
		add = 250
	}
	else_if = {
		limit = {
			race_has_200_year_lifespan = yes	#dwarves
		}
		add = 200
	}
	else_if = {
		limit = {
			race_has_100_year_lifespan = yes	#half-elves, harimari, etc
		}
		add = 100
	}
	else_if = {
		limit = {
			race_has_average_year_lifespan = yes
		}
		add = 80
	}
	else_if = {
		limit = {
			race_has_50_year_lifespan = yes	#gnolls and harpies
		}
		add = 50	#its actually 15.5 but lets be kinder
	}
	else = {
		add = 80	#default to it
	}

	divide = 80
}

age_racial_standard = {
	value = age
	divide = calculate_age_ratio
	round = yes
}

# # I want one function that passes in an age, and does the calc
# age_racial_equivalent = {
# 	value = calculate_age_ratio
# 	multiply = $AGE$
# 	round = yes
# }

# # you can't do it in scripted values but this doesn't work either
# save_scope_value_as = {
# 	name = age_ratio
# 	value = {
# 		value = calculate_age_ratio
# 		multiply = $AGE$
# 		round = yes
# 	}
# }

age_10_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 20
	round = yes
}

age_20_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 20
	round = yes
}

age_24_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 24
	round = yes
}

age_25_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 25
	round = yes
}

age_29_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 29
	round = yes
}

age_30_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 30
	round = yes
}

age_33_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 33
	round = yes
}

age_35_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 35
	round = yes
}

age_36_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 36
	round = yes
}

age_40_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 40
	round = yes
}

age_45_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 45
	round = yes
}

age_48_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 48
	round = yes
}

age_50_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 50
	round = yes
}

age_55_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 55
	round = yes
}

age_60_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 60
	round = yes
}

age_65_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 65
	round = yes
}

age_70_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 70
	round = yes
}

age_80_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 80
	round = yes
}

age_90_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 90
	round = yes
}

age_100_racial_equivalent = {
	value = calculate_age_ratio
	multiply = 100
	round = yes
}
