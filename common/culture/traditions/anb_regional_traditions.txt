################################
# REGIONAL CULTURAL TRADITIONS #
################################

tradition_great_men_great_mustachios = {	#from eu4 lol
	category = regional

	layers = {
		0 = intrigue
		1 = mediterranean
		4 = crown.dds
	}
	
	is_shown = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:vernid
				this = culture:vernman
			}
		}
	}
	can_pick = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:vernid
				this = culture:vernman
			}
		}
	}
	
	parameters = {
	
	}

	character_modifier = {

		stress_gain_mult = 0.2
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_communal }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_courtly_or_communal_desc
				}
			}
		}
	}

	ai_will_do = { value = 100 }
}

tradition_drive_for_greatness = {
	category = societal

	layers = {
		0 = diplo
		1 = western
		4 = crown.dds
	}
	
	parameters = {
		ambitious_trait_more_common = yes
	}
	character_modifier = {
		stress_gain_mult = 0.05
		monthly_lifestyle_xp_gain_mult = 0.1
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_communal }		#PLACEHOLDER, NEEDS EDITING
						any_in_list = { list = traits this = flag:ethos_egalitarian }
						any_in_list = { list = traits this = flag:ethos_stoic }			#PLACEHOLDER, NEEDS EDITING
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_communal_egalitarian_or_stoic_desc					#PLACEHOLDER, NEEDS EDITING
				}
			}
			if = {
				limit = {
					trigger_if = {
						limit = {
							scope:character = {
								is_ai = no
							}
						}
						NOT = {
							any_ruler = {
								count >= 5
								culture = prev
								primary_title.tier >= tier_county
								has_trait = ambitious
							}
						}
					}
					trigger_else = {
						always = no
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = rulers_with_ambitious_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	ai_will_do = {
		value = 100
		if = {
			limit = {
				OR = {
					any_in_list = { list = traits this = flag:tradition_welcoming }
					any_in_list = { list = traits this = flag:tradition_eye_for_an_eye }
					any_in_list = { list = traits this = flag:tradition_zealous_people }
					any_in_list = { list = traits this = flag:tradition_forbearing }
					any_in_list = { list = traits this = flag:tradition_equitable }
					any_in_list = { list = traits this = flag:tradition_charitable }
					any_in_list = { list = traits this = flag:tradition_modest }
				}
			}
			multiply = 0.1
		}
	}
}

tradition_garden_of_surakes = {	
	category = regional

	layers = {
		0 = steward
		1 = mediterranean
		4 = farmland.dds
	}
	
	is_shown = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:zanite
				this = culture:brasanni
				this = culture:surani
				this = culture:barsibu
				this = culture:akalsesi
			}
		}
	}
	can_pick = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:zanite
				this = culture:brasanni
				this = culture:surani
				this = culture:barsibu
				this = culture:akalsesi
			}
		}
	}
	
	parameters = {
		enable_garden_duchy_building = yes
		can_appoint_court_gardener = yes
		can_recruit_gardeners = yes
	}
	character_modifier = {
		
	}
	county_modifier = {
		farmlands_development_growth_factor = 0.2
		farmlands_construction_gold_cost = -0.2
		floodplains_development_growth_factor = 0.2
		floodplains_construction_gold_cost = -0.2
		farmlands_levy_size = 0.1
		floodplains_levy_size = 0.1
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_communal }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_courtly_or_communal_desc
				}
			}
		}
	}

	ai_will_do = { value = 100 }
}

tradition_precursor_legions = {
	category = regional

	layers = {
		0 = martial
		1 = mena
		4 = laurel.dds
	}
	
	is_shown = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:sun_elvish
			}
		}
	}
	can_pick = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:sun_elvish
			}
		}
	}
	
	character_modifier = {
		levy_size = -0.35
		men_at_arms_recruitment_cost = -0.25
		men_at_arms_maintenance = -0.35
		levy_reinforcement_rate = -0.35
		men_at_arms_cap = 1
	}

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_communal }
						any_in_list = { list = traits this = flag:ethos_spiritual }
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_communal_spiritual_or_stoic_desc
				}
			}
		}
	}

	ai_will_do = { value = 100 }
}

tradition_land_of_authority = {	
	category = regional

	layers = {
		0 = martial
		1 = mediterranean
		4 = soldiers.dds
	}
	
	is_shown = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:mukarron
				this = culture:taafi
			}
		}
	}
	can_pick = {
		any_parent_culture_or_above = {
			OR = {
				this = culture:mukarron
				this = culture:taafi
			}
		}
	}
	
	parameters = {
	}
	character_modifier = {
		dread_gain_mult = 0.20
		dread_per_tyranny_add = 0.33
		men_at_arms_maintenance_per_dread_mult = -0.10
		intimidated_vassal_tax_contribution_mult = 0.10
	}
	county_modifier = {
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_communal }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_courtly_or_communal_desc
				}
			}
		}
	}

	ai_will_do = { value = 100 }
}