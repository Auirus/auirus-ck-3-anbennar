﻿tradition_precursor_legions = {
	category = combat

	layers = {
		0 = martial
		1 = mediterranean
		4 = laurel.dds
	}

	is_shown = {
		has_cultural_pillar = heritage_elven
	}
	can_pick = {
	}
	
	province_modifier = {
	}
	character_modifier = {
		levy_size = -0.35
		men_at_arms_recruitment_cost = -0.25
		men_at_arms_maintenance = -0.25
		levy_reinforcement_rate = -0.35
		hard_casualty_modifier = -0.15
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_egalitarian }
						any_in_list = { list = traits this = flag:ethos_courtly }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_egalitarian_or_courtly_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
	}
}