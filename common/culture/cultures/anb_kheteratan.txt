﻿kheteratan = {
	color = { 219 148 35 }

	ethos = ethos_spiritual
	heritage = heritage_kheteratan
	language = language_kheteratan
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_agrarian
		tradition_zealous_people
		tradition_faith_bound
		tradition_legalistic
	}
	
	name_list = name_list_kheteratan
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = kheteratan_lower
	}
}

irsmahapan = {
	color = { 219 217 35 }

	ethos = ethos_bellicose
	heritage = heritage_kheteratan
	language = language_kheteratan
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_warriors_of_the_dry
		tradition_martial_admiration
		tradition_land_of_the_bow
	}
	
	name_list = name_list_kheteratan
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = kheteratan_upper
	}
}

shasoura = {
	color = { 201 173 165 }

	ethos = ethos_spiritual
	heritage = heritage_kheteratan
	language = language_kheteratan
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_xenophilic
		tradition_music_theory
		tradition_desert_nomads
	}
	
	name_list = name_list_kheteratan
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}

tijarikheti = {
	stoic = { 176 115 10 }

	ethos = ethos_stoic
	heritage = heritage_kheteratan
	language = language_kheteratan
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_highland_warriors
		tradition_metal_craftsmanship
		tradition_mountaineers
	}
	
	name_list = name_list_kheteratan
	
	coa_gfx = { berber_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = kheteratan_upper
	}
}