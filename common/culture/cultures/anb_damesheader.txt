﻿old_damerian = {
	color = { 16 78 88 }
	
	ethos = ethos_egalitarian
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_caravaneers
		tradition_parochialism
		tradition_family_entrepreneurship
	}
	
	name_list = name_list_old_damerian
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_coa_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		5 = caucasian_ginger
		30 = caucasian_brown_hair
		20 = caucasian_dark_hair
		15 = mediterranean
	}
}

damerian = {
	color = "damerian_blue"
	created = 1000.1.1
	parents = { old_damerian moon_elvish }

	ethos = ethos_egalitarian
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_drive_for_greatness
		tradition_xenophilic
		tradition_astute_diplomats
		tradition_talent_acquisition
		tradition_longbow_competitions
	}
	
	name_list = name_list_damerian
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_coa_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		5 = caucasian_ginger
		30 = caucasian_brown_hair
		20 = caucasian_dark_hair
		15 = mediterranean
	}
}

lenco_damerian = {
	color = "lorentish_lilac"
	created = 700.1.1
	parents = { old_damerian lorenti }

	ethos = ethos_egalitarian
	heritage = heritage_damesheader
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_culinary_art
		tradition_formation_fighting
		tradition_longbow_competitions
	}
	
	name_list = name_list_old_damerian
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_coa_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond	
		20 = caucasian_ginger	#from lorentish origin
		50 = caucasian_brown_hair
		10 = caucasian_dark_hair
	}
}

carnetori = {
	color = { 0 233 117 }

	ethos = ethos_egalitarian
	heritage = heritage_damesheader
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_forest_folk
		tradition_wedding_ceremonies
	}
	
	name_list = name_list_carnetori
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_coa_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		10 = caucasian_blond
		20 = caucasian_ginger
		40 = caucasian_brown_hair
		30 = caucasian_dark_hair
	}
}

vernman = {
	color = "verne_red"
	created = 1000.1.1
	parents = { vernid }

	ethos = ethos_bellicose
	heritage = heritage_damesheader
	language = language_vernid_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_swords_for_hire
		tradition_martial_admiration
		tradition_reverence_for_veterans
		tradition_great_men_great_mustachios
	}
	
	name_list = name_list_vernman
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		10 = caucasian_blond_vernmen
		20 = caucasian_brown_hair_vernmen
		30 = caucasian_dark_hair_vernmen
		40 = mediterranean_vernmen	#mustachio variant of vanilla mediterranean
	}
}

vernid = {
	color = { 117 30 4 }

	ethos = ethos_bellicose
	heritage = heritage_damesheader
	language = language_vernid_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_great_men_great_mustachios
	}
	
	name_list = name_list_vernid
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		10 = caucasian_blond_vernmen
		20 = caucasian_brown_hair_vernmen
		30 = caucasian_dark_hair_vernmen
		40 = mediterranean_vernmen	#mustachio variant of vanilla mediterranean
	}
}

tretunic = {
	color = { 187  122  87 }

	ethos = ethos_courtly
	heritage = heritage_damesheader
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fishermen
		tradition_esteemed_hospitality
	}
	
	name_list = name_list_damerian
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond	
		20 = caucasian_ginger	#from lorentish origin
		50 = caucasian_brown_hair
		10 = caucasian_dark_hair
	}
}

exwesser = {
	color = { 125  182  192 }

	ethos = ethos_communal
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fishermen
		tradition_longbow_competitions
	}
	
	name_list = name_list_old_damerian
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		25 = caucasian_blond	
		5 = caucasian_ginger
		50 = caucasian_brown_hair
		20 = caucasian_dark_hair
	}
}

pearlsedger = {
	color = { 255 255 255 }
	created = 879.1.1	#canon founding of pearlsedge by Henrik Divenscourge
	parents = { tretunic dalric }

	ethos = ethos_bellicose
	heritage = heritage_damesheader
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_astute_diplomats
		tradition_seafaring
	}
	
	name_list = name_list_pearlsedger
	
	coa_gfx = { danish_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		25 = caucasian_northern_blond_pearlsedger
		5 = caucasian_northern_ginger_pearlsedger
		50 = caucasian_northern_brown_hair_pearlsedger
		20 = caucasian_northern_dark_hair_pearlsedger
	}
}

esmari = {
	color = { 241 174 118 }
	created = 1020.1.1 #placeholder
	parents = { old_esmari moon_elvish }

	ethos = ethos_communal
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fervent_temple_builders
		tradition_agrarian
		tradition_festivities
	}
	
	name_list = name_list_esmari
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		5 = caucasian_ginger
		60 = caucasian_brown_hair
		15 = caucasian_dark_hair
	}
}

old_esmari = {
	color = { 178 118 85 }

	ethos = ethos_spiritual
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
	}
	
	name_list = name_list_old_esmari
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		5 = caucasian_ginger
		60 = caucasian_brown_hair
		15 = caucasian_dark_hair
	}
}

ryalani = {
	color = { 205 69 143 }

	ethos = ethos_spiritual
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_fervent_temple_builders
		tradition_wedding_ceremonies
	}
	
	name_list = name_list_old_esmari
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		20 = caucasian_blond
		5 = caucasian_ginger
		60 = caucasian_brown_hair
		15 = caucasian_dark_hair
	}
}

arannese = {
	color = { 31 55 152 }
	created = 1100.1.1
	parents = { roilsardi milcori }

	ethos = ethos_bellicose
	heritage = heritage_damesheader
	language = language_aldresian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_legalistic
		tradition_fp2_strategy_gamers
		tradition_formation_fighting
		tradition_medicinal_plants
	}
	
	name_list = name_list_arannese
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		80 = milcorissian
		10 = slavic_dark_hair
		10 = caucasian_dark_hair
	}
}

crownsman = {
	color = { 221  198  129 }

	ethos = ethos_bureaucratic
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_legalistic
		tradition_astute_diplomats
	}
	
	name_list = name_list_crownsman
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		80 = caucasian_blond
		10 = caucasian_brown_hair
		10 = caucasian_dark_hair
	}
}


arbarani = {
	color = { 74  131  101 }
	created = 1100.1.1
	parents = { crownsman gawedi }

	ethos = ethos_stoic
	heritage = heritage_damesheader
	language = language_damerian_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_pastoralists
		tradition_talent_acquisition
	}
	
	name_list = name_list_arbarani
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx dde_hre_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		50 = caucasian_blond
		10 = caucasian_brown_hair
		40 = caucasian_dark_hair
	}
}