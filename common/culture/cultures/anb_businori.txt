﻿busilari = {
	color = { 211 85 4 }
	created = 1000.1.1
	parents = { businori ndurubu }	#Anbennar TODO - busilar should start businori, and this busilari hybrid is after lion-hearted tribe comes

	ethos = ethos_stoic
	heritage = heritage_businori
	language = language_businori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_mystical_ancestors
		tradition_ancient_miners
		tradition_adaptive_skirmishing
	}
	
	name_list = name_list_busilari

	coa_gfx = { western_coa_gfx } 
	building_gfx = { western_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 		

	ethnicities = {
		90 = mediterranean
		20 = african
	}
}

businori = {
	color = { 111 85 4 }

	ethos = ethos_stoic
	heritage = heritage_businori
	language = language_businori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_ancient_miners
		tradition_fp2_malleable_subjects
		tradition_hill_dwellers
	}
	
	name_list = name_list_businori

	coa_gfx = { western_coa_gfx } 
	building_gfx = { western_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 		

	ethnicities = {
		100 = mediterranean
	}
}

tefori = {
	color = { 207 182 100 }

	ethos = ethos_spiritual
	heritage = heritage_businori
	language = language_businori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_city_keepers
		tradition_stalwart_defenders
	}
	
	name_list = name_list_businori

	coa_gfx = { western_coa_gfx } 
	building_gfx = { western_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 		

	ethnicities = {
		100 = mediterranean
	}
}

eborthili = {
	color = { 243 219 138 }

	ethos = ethos_courtly
	heritage = heritage_businori
	language = language_businori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_maritime_mercantilism
		tradition_seafaring
		tradition_parochialism
	}
	
	name_list = name_list_businori

	coa_gfx = { western_coa_gfx } 
	building_gfx = { western_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 		

	ethnicities = {
		100 = mediterranean
	}
}

crathanori = {
	color = { 8 112 214 }
	created = 1100.1.1	#not canon date, but they pop up as Jexis moves Busilari into that region
	parents = { busilari yametsesi }

	ethos = ethos_communal
	heritage = heritage_businori
	language = language_businori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_parochialism
		tradition_religion_blending
		tradition_fp2_malleable_subjects
	}
	
	name_list = name_list_businori

	coa_gfx = { western_coa_gfx } 
	building_gfx = { western_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 		

	ethnicities = {
		75 = mediterranean
		25 = mediterranean_byzantine
	}
}

milcori = {
	color = { 91 135 192 }

	ethos = ethos_spiritual
	heritage = heritage_businori
	language = language_milcori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_monogamous
		tradition_medicinal_plants
		tradition_mystical_ancestors
	}
	
	name_list = name_list_milcori

	coa_gfx = { western_coa_gfx } 
	building_gfx = { western_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 		

	ethnicities = {
		100 = milcorissian
	}
}