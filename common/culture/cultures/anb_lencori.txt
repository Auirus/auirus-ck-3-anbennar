﻿lorenti = {
	color = "lorentish_green"

	ethos = ethos_communal
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_chanson_de_geste # Needs to be reflavoured to Lorentish name/desc
		tradition_storytellers
		tradition_horse_breeder
	}
	
	name_list = name_list_lorenti
	
	coa_gfx = { frankish_group_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		10 = caucasian_blond
		5 = caucasian_ginger
		45 = caucasian_brown_hair
		35 = caucasian_dark_hair
	}
}

lorentish = {
	color = "lorentish_red"
	created = 1000.1.1			#placeholder for a more accurate date
	parents = { lorenti moon_elvish }

	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_chanson_de_geste # Needs to be reflavoured to Lorentish name/desc
		tradition_martial_admiration
		tradition_culinary_art
		tradition_astute_diplomats
	}
	
	name_list = name_list_lorentish
	
	coa_gfx = { french_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		5 = caucasian_blond
		30 = caucasian_ginger
		60 = caucasian_brown_hair
		5 = caucasian_dark_hair
	}
}

roilsardi = {
	color = "roilsardi_red"

	ethos = ethos_bellicose
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_family_entrepreneurship
		tradition_formation_fighting
		tradition_quarrelsome
	}
	
	name_list = name_list_roilsardi
	
	coa_gfx = { french_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		20 = caucasian_ginger
		65 = caucasian_brown_hair
		15 = caucasian_dark_hair
	}
}

derannic = {
	color = "derannic_pink"
	created = 860.1.1	#canon
	parents = { lorenti olavish }

	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_chivalry
		tradition_seafaring
	}
	
	name_list = name_list_derannic
	
	coa_gfx = { norman_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		25 = caucasian_blond
		10 = caucasian_ginger
		30 = caucasian_brown_hair
		15 = caucasian_northern_blond
		5 = caucasian_northern_ginger
		15 = caucasian_northern_brown_hair
	}
}

sorncosti = {
	color = { 135 3 86 }
	created = 1020.1.1			#placeholder for a more accurate date
	parents = { sormanni moon_elvish }

	ethos = ethos_stoic
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hill_dwellers
		tradition_culinary_art
	}
	
	name_list = name_list_sorncosti
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		10 = caucasian_blond
		5 = caucasian_ginger
		35 = caucasian_brown_hair
		50 = caucasian_dark_hair
	}
}

sormanni = {
	color = { 81 2 52 }

	ethos = ethos_stoic
	heritage = heritage_lencori
	language = language_lencori_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hill_dwellers
	}
	
	name_list = name_list_sormanni
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		50 = caucasian_brown_hair
		50 = caucasian_dark_hair
	}
}

iochander = {
	color = { 173 61 135 }

	ethos = ethos_courtly
	heritage = heritage_lencori
	language = language_gnomish
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_loyal_soldiers
		tradition_language_scholars
	}
	
	name_list = name_list_iochander
	
	coa_gfx = { occitan_coa_gfx western_coa_gfx }
	building_gfx = { mediterranean_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		30 = caucasian_blond
		10 = caucasian_ginger
		50 = caucasian_brown_hair
		10 = caucasian_dark_hair
	}
}