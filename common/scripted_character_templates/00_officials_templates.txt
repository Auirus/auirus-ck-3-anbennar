﻿
tax_collector_character = {
	age = { 30 65 }
	gender_female_chance = root_faith_dominant_gender_female_chance
	random_traits = yes
	culture = scope:county.culture
	faith = scope:county.faith
	stewardship = {
		min_template_decent_skill
		max_template_decent_skill
	}
	
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
	}	
}

estate_owner_character = {
	age = { 20 40 }
	trait = deceitful
	random_traits_list = {
		count = 1
		education_stewardship_3 = {}
		education_stewardship_4 = {}
	}
	random_traits = yes
	culture = root.capital_province.culture
	faith = root.capital_province.faith
	gender_female_chance = {
		if = {
			limit = { root.capital_province.faith = { has_doctrine = doctrine_gender_male_dominated } }
			add = 0
		}
		else_if = {
			limit = { root.capital_province.faith = { has_doctrine = doctrine_gender_female_dominated } }
			add = 100
		}
		else = {
			add = 50
		}
	}
	stewardship = {
		min_template_decent_skill
		max_template_decent_skill
	}
	
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
	}
}