﻿

special_genes = {


accessory_genes = {

#####################################
#									#
# 		  	   PROPS  		        #
#									#
#####################################



	props = {
		no_props = {
			index = 0
			male = {
			}
			female = {
			}
			boy = {
			}
			girl = {
			}
		}
        grid = {
            index = 1
            male = {
                1 = debug_grid_01
            }
            female = male
            boy = male
            girl = male
        }
		prison_shackles = {
			index = 2
			male = {
				1 = shackles_arrest_01
			}
			female = {
				1 = shackles_arrest_01
			}
			boy = {
				1 = shackles_arrest_01
			}
			girl = {
				1 = shackles_arrest_01
			}
		}
		prison_shackles_02 = {
			index = 3
			male = {
				1 = shackles_dungeon_01
			}
			female = {
				1 = shackles_dungeon_01
			}
			boy = {
				1 = shackles_dungeon_01
			}
			girl = {
				1 = shackles_dungeon_01
			}
		}
		spymaster_daggers = {
			index = 4
			male = {
				1 = western_dagger_01
			}
			female = {
				1 = western_dagger_01
			}
			boy = male
			girl = female
		}
		rondel_dagger = {
			index = 92
			male = {
				1 = western_dagger_02
			}
			female = {
				1 = western_dagger_02
			}
			boy = male
			girl = female
		}
		spymaster_daggers_mena = {
			index = 5
			male = {
				1 = mena_dagger_01
			}
			female = {
				1 = mena_dagger_01
			}
			boy = male
			girl = female
		}
		spymaster_daggers_indian = {
			index = 6
			male = {
				1 = indian_dagger_01
			}
			female = {
				1 = indian_dagger_01
			}
			boy = male
			girl = female
		}
		marshal_swords = {
			index = 7
			male = {
				1 = western_sword_02
			}
			female = {
				1 = western_sword_02
			}
			boy = male
			girl = female
		}
		steward_pouches = {
			index = 8
			male = {
				1 = western_pouch_02
			}
			female = {
				1 = western_pouch_02
			
			}
			boy = male
			girl = female
		}
		chancellor_scrolls = {
			index = 9
			male = {
				1 = western_scroll_01
			}
			female = {
				1 = western_scroll_01
			}
			boy = male
			girl = female
		}
		swaddled_baby = {
			index = 10
			male = {
				1 = prop_swaddled_baby_01
			}
			female = {
				1 = prop_swaddled_baby_01
			}
			boy = male
			girl = female
		}
		prophet_shield = {
			index = 11
			male = {
				1 = prophet_shield
			}
			female = {
				1 = prophet_shield
			}
			boy = male
			girl = female
		}
		shackles_dungeon_01 = {
			index = 12
			male = {
				1 = shackles_dungeon_01
			}
			female = {
				1 = shackles_dungeon_01
			}
			boy = {
			}
			girl = {
			}
		}
		shackles_arrest_01 = {
			index = 13
			male = {
				1 = shackles_arrest_01
			}
			female = {
				1 = shackles_arrest_01
			}
			boy = {
			}
			girl = {
			}
		}
		fp1_marshal_sword = {
			index = 14
			male = {
				1 = fp1_marshal_sword_01
			}
			female = {
				1 = fp1_marshal_sword_01
			}
			boy = male
			girl = female
		}
		fp1_spymasters_dagger = {
			index = 15
			male = {
				1 = fp1_spymasters_dagger_01
			}
			female = {
				1 = fp1_spymasters_dagger_01			
			}
			boy = male
			girl = female
		}
		fp1_rune_tablet_01 = {
			index = 16
			male = {
				1 = fp1_rune_tablet_01
			}
			female = {
				1 = fp1_rune_tablet_01		
			}
			boy = male
			girl = female
		}
		messenger_scroll = {
			index = 17
			male = {
				1 = rolled_scroll_01
			}
			female = {
				1 = rolled_scroll_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_spear_left_hand = {
			index = 18
			male = {
				1 = ep1_spear_01_left_hand
			}
			female = {
				1 = ep1_spear_01_left_hand		
			}
			boy = male
			girl = female
		}
		ep1_artisan_spear_right_hand = {
			index = 19
			male = {
				1 = ep1_spear_01_right_hand
			}
			female = {
				1 = ep1_spear_01_right_hand		
			}
			boy = male
			girl = female
		}
		ep1_mena_artisan_spear_left_hand = {
			index = 20
			male = {
				1 = ep1_mena_spear_01_left_hand
			}
			female = {
				1 = ep1_mena_spear_01_left_hand		
			}
			boy = male
			girl = female
		}
		ep1_mena_artisan_spear_right_hand = {
			index = 21
			male = {
				1 = ep1_mena_spear_01_right_hand
			}
			female = {
				1 = ep1_mena_spear_01_right_hand		
			}
			boy = male
			girl = female
		}
		ep1_african_artisan_spear_left_hand = {
			index = 22
			male = {
				1 = ep1_african_spear_01_left_hand
			}
			female = {
				1 = ep1_african_spear_01_left_hand		
			}
			boy = male
			girl = female
		}
		ep1_african_artisan_spear_right_hand = {
			index = 23
			male = {
				1 = ep1_african_spear_01_right_hand
			}
			female = {
				1 = ep1_african_spear_01_right_hand		
			}
			boy = male
			girl = female
		}
		ep1_indian_artisan_spear_left_hand = {
			index = 24
			male = {
				1 = ep1_indian_spear_01_left_hand
			}
			female = {
				1 = ep1_indian_spear_01_left_hand		
			}
			boy = male
			girl = female
		}
		ep1_indian_artisan_spear_right_hand = {
			index = 25
			male = {
				1 = ep1_indian_spear_01_right_hand
			}
			female = {
				1 = ep1_indian_spear_01_right_hand		
			}
			boy = male
			girl = female
		}
		ep1_artisan_sword_mena = {
			index = 26
			male = {
				1 = ep1_sword_mena_01
			}
			female = {
				1 = ep1_sword_mena_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_sword_indian = {
			index = 27
			male = {
				1 = ep1_sword_indian_01
			}
			female = {
				1 = ep1_sword_indian_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_sword_byzantine = {
			index = 28
			male = {
				1 = ep1_sword_byzantine_01
			}
			female = {
				1 = ep1_sword_byzantine_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_sword_steppe = {
			index = 29
			male = {
				1 = ep1_sword_steppe_01
			}
			female = {
				1 = ep1_sword_steppe_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_sword_african = {
			index = 30
			male = {
				1 = ep1_sword_african_01
			}
			female = {
				1 = ep1_sword_african_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_sword_northern = {
			index = 31
			male = {
				1 = ep1_sword_northern_01
			}
			female = {
				1 = ep1_sword_northern_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe_mena = {
			index = 32
			male = {
				1 = ep1_axe_mena_01
			}
			female = {
				1 = ep1_axe_mena_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe = {
			index = 33
			male = {
				1 = ep1_axe_01
			}
			female = {
				1 = ep1_axe_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe_northern_pagan = {
			index = 34
			male = {
				1 = ep1_axe_northern_pagan_01
			}
			female = {
				1 = ep1_axe_northern_pagan_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe_indian = {
			index = 35
			male = {
				1 = ep1_axe_indian_01
			}
			female = {
				1 = ep1_axe_indian_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe_african = {
			index = 36
			male = {
				1 = ep1_axe_african_01
			}
			female = {
				1 = ep1_axe_african_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe_steppe = {
			index = 37
			male = {
				1 = ep1_axe_steppe_01
			}
			female = {
				1 = ep1_axe_steppe_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_axe_mediterranean = {
			index = 38
			male = {
				1 = ep1_axe_mediterranean_01
			}
			female = {
				1 = ep1_axe_mediterranean_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_mace_african = {
			index = 39
			male = {
				1 = ep1_mace_african_01
			}
			female = {
				1 = ep1_mace_african_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_mace_western = {
			index = 40
			male = {
				1 = ep1_mace_western_01
			}
			female = {
				1 = ep1_mace_western_01		
			}
			boy = male
			girl = female
		}
		ep1_artisan_mace_steppe = {
			index = 41
			male = {
				1 = ep1_mace_steppe_01
			}
			female = {
				1 = ep1_mace_steppe_01	
			}
			boy = male
			girl = female
		}
		ep1_artisan_mace_byzantine = {
			index = 42
			male = {
				1 = ep1_mace_byzantine_01
			}
			female = {
				1 = ep1_mace_byzantine_01	
			}
			boy = male
			girl = female
		}
		ep1_artisan_mace_mena = {
			index = 88
			male = {
				1 = ep1_mace_mena_01
			}
			female = {
				1 = ep1_mace_mena_01
			}
			boy = male
			girl = female
		}
		ep1_artisan_mace_indian = {
			index = 89
			male = {
				1 = ep1_mace_indian_01
			}
			female = {
				1 = ep1_mace_indian_01
			}
			boy = male
			girl = female
		}
		ep1_artisan_hammer_mena = {
			index = 90
			male = {
				1 = ep1_hammer_mena_01
			}
			female = {
				1 = ep1_hammer_mena_01
			}
			boy = male
			girl = female
		}
		ep1_artisan_hammer_western = {
			index = 91
			male = {
				1 = ep1_hammer_western_01
			}
			female = {
				1 = ep1_hammer_western_01
			}
			boy = male
			girl = female
		}
		# Genes for props with overrides for equipped artifacts
		ep1_artifact_axe_equipped = {
			index = 43
			male = {
				1 = ep1_artifact_axe_equipped_01
			}
			female = {
				1 = ep1_artifact_axe_equipped_01		
			}
			boy = male
			girl = female
		}
		ep1_artifact_mace_equipped = {
			index = 44
			male = {
				1 = ep1_artifact_mace_equipped_01
			}
			female = {
				1 = ep1_artifact_mace_equipped_01		
			}
			boy = male
			girl = female
		}
		ep1_artifact_sword_equipped = {
			index = 45
			male = {
				1 = ep1_artifact_sword_equipped_01
			}
			female = {
				1 = ep1_artifact_sword_equipped_01		
			}
			boy = male
			girl = female
		}
		ep1_artifact_dagger_equipped = {
			index = 46
			male = {
				1 = ep1_artifact_dagger_equipped_01
			}
			female = {
				1 = ep1_artifact_dagger_equipped_01		
			}
			boy = male
			girl = female
		}
		ep1_artifact_spear_equipped = {
			index = 47
			male = {
				1 = ep1_artifact_spear_equipped_01
			}
			female = {
				1 = ep1_artifact_spear_equipped_01		
			}
			boy = male
			girl = female
		}
		ep1_artifact_spear_equipped_left = {
			index = 93
			male = {
				1 = ep1_artifact_spear_equipped_01_left
			}
			female = {
				1 = ep1_artifact_spear_equipped_01_left		
			}
			boy = male
			girl = female
		}
		ep1_mena_book_big = {
			index = 48
			male = {
				1 = ep1_mena_book_big_01
			}
			female = {
				1 = ep1_mena_book_big_01		
			}
			boy = male
			girl = female
		}
		ep1_western_book_big = {
			index = 49
			male = {
				1 = ep1_western_book_big_01
			}
			female = {
				1 = ep1_western_book_big_01		
			}
			boy = male
			girl = female
		}
		ep1_tool_quill = {
			index = 50
			male = {
				1 = ep1_tool_quill_01
			}
			female = {
				1 = ep1_tool_quill_01		
			}
			boy = male
			girl = female
		}
		ep1_artifact_hammer_equipped = {
			index = 51
			male = {
				1 = ep1_artifact_hammer_equipped_01
			}
			female = {
				1 = ep1_artifact_hammer_equipped_01		
			}
			boy = male
			girl = female
		}
		fp2_iberian_muslim_dagger = {
			index = 52
			male = {
				1 = fp2_iberian_muslim_dagger_01
			}
			female = {
				1 = fp2_iberian_muslim_dagger_01		
			}
			boy = male
			girl = female
		}
		fp2_iberian_muslim_sword = {
			index = 53
			male = {
				1 = fp2_iberian_muslim_sword_01
			}
			female = {
				1 = fp2_iberian_muslim_sword_01	
			}
			boy = male
			girl = female
		}
		fp2_iberian_christian_sword = {
			index = 54
			male = {
				1 = fp2_iberian_christian_sword_01
			}
			female = {
				1 = fp2_iberian_christian_sword_01	
			}
			boy = male
			girl = female
		}
		fp2_iberian_muslim_scroll = {
			index = 55
			male = {
				1 = fp2_iberian_muslim_scroll_01
			}
			female = {
				1 = fp2_iberian_muslim_scroll_01		
			}
			boy = male
			girl = female
		}
		fp2_steward_pouch = {
			index = 56
			male = {
				1 = fp2_steward_pouch_01
			}
			female = {
				1 = fp2_steward_pouch_01		
			}
			boy = male
			girl = female
		}
		bp1_lantern = {
			index = 57
			male = {
				1 = bp1_lantern
			}
			female = {
				1 = bp1_lantern	
			}
			boy = male
			girl = female
		}
		bp1_tankard = {
			index = 58
			male = {
				1 = bp1_tankard
			}
			female = {
				1 = bp1_tankard
			}
			boy = male
			girl = female
		}
		bp1_goblet = {
			index = 59
			male = {
				1 = bp1_goblet
			}
			female = {
				1 = bp1_goblet
			}
			boy = male
			girl = female
		}
		fp2_chess_piece_left_hand = {
			index = 60
			male = {
				1 = fp2_steward_chess_piece_01_left_hand
			}
			female = {
				1 = fp2_steward_chess_piece_01_left_hand		
			}
			boy = male
			girl = female
		}
		fp2_chess_piece_right_hand = {
			index = 61
			male = {
				1 = fp2_steward_chess_piece_01_right_hand
			}
			female = {
				1 = fp2_steward_chess_piece_01_right_hand		
			}
			boy = male
			girl = female
		}
		ep2_crossbow_right = {
			index = 62
			male = {
				1 = ep2_crossbow_01_right_hand
			}
			female = {
				1 = ep2_crossbow_01_right_hand		
			}
			boy = male
			girl = female
		}
		ep2_horn_left = {
			index = 63
			male = {
				1 = ep2_horn_01_left_hand
			}
			female = {
				1 = ep2_horn_01_left_hand		
			}
			boy = male
			girl = female
		}
		ep2_rabbit_carcass_left = {
			index = 64
			male = {
				1 = ep2_rabbit_carcass_01_left_hand
			}
			female = {
				1 = ep2_rabbit_carcass_01_left_hand		
			}
			boy = male
			girl = female
		}
		ep2_hunting_knife_right = {
			index = 65
			male = {
				1 = ep2_hunting_knife_01_right_hand
			}
			female = {
				1 = ep2_hunting_knife_01_right_hand		
			}
			boy = male
			girl = female
		}	
		ep2_hunting_sword_right = {
			index = 75
			male = {
				1 = ep2_hunting_sword_01
			}
			female = {
				1 = ep2_hunting_sword_01		
			}
			boy = male
			girl = female
		}	
		ep2_western_longsword_01_right = {
			index = 76
			male = {
				1 = ep2_western_longsword_01_a
				1 = ep2_western_longsword_01_b
			}
			female = {
				1 = ep2_western_longsword_01_a	
				1 = ep2_western_longsword_01_b			
			}
			boy = male
			girl = female
		}	
		#bow closed
		ep2_steppe_bow_01_a_left = {
			index = 66
			male = {
				1 = ep2_steppe_bow_01_a_left_hand
			}
			female = {
				1 = ep2_steppe_bow_01_a_left_hand		
			}
			boy = male
			girl = female
		}	
		ep2_mediterranean_bow_01_a_left = {
			index = 67
			male = {
				1 = ep2_mediterranean_bow_01_a_left_hand
			}
			female = {
				1 = ep2_mediterranean_bow_01_a_left_hand		
			}
			boy = male
			girl = female
		}	
		ep2_indian_bow_01_a_left = {
			index = 68
			male = {
				1 = ep2_indian_bow_01_a_left_hand
			}
			female = {
				1 = ep2_indian_bow_01_a_left_hand		
			}
			boy = male
			girl = female
		}
		ep2_long_bow_01_a_left = {
			index = 69
			male = {
				1 = ep2_long_bow_01_a_left_hand
			}
			female = {
				1 = ep2_long_bow_01_a_left_hand		
			}
			boy = male
			girl = female
		}	
		ep2_short_bow_01_left = {
			index = 70
			male = {
				1 = ep2_short_bow_01_a_left_hand
				1 = ep2_short_bow_01_b_left_hand
			}
			female = {
				1 = ep2_short_bow_01_a_left_hand
				1 = ep2_short_bow_01_b_left_hand
			}
			boy = male
			girl = female
		}	
		# bow drawn/aiming
		ep2_steppe_bow_01_a_drawn_left = {
			index = 71
			male = {
				1 = ep2_steppe_bow_01_a_drawn_left_hand
			}
			female = {
				1 = ep2_steppe_bow_01_a_drawn_left_hand		
			}
			boy = male
			girl = female
		}	
		ep2_mediterranean_bow_01_a_drawn_left = {
			index = 72
			male = {
				1 = ep2_mediterranean_bow_01_a_drawn_left_hand
			}
			female = {
				1 = ep2_mediterranean_bow_01_a_drawn_left_hand		
			}
			boy = male
			girl = female
		}
		ep2_indian_bow_01_a_drawn_left = {
			index = 73
			male = {
				1 = ep2_indian_bow_01_a_drawn_left_hand
			}
			female = {
				1 = ep2_indian_bow_01_a_drawn_left_hand		
			}
			boy = male
			girl = female
		}
		ep2_long_bow_01_a_drawn_left = {
			index = 74
			male = {
				1 = ep2_long_bow_01_a_drawn_left_hand
			}
			female = {
				1 = ep2_long_bow_01_a_drawn_left_hand		
			}
			boy = male
			girl = female
		}
		ep2_short_bow_01_drawn_left = {
			index = 80
			male = {
				1 = ep2_short_bow_01_a_drawn_left_hand
				1 = ep2_short_bow_01_b_drawn_left_hand
			}
			female = {
				1 = ep2_short_bow_01_a_drawn_left_hand
				1 = ep2_short_bow_01_b_drawn_left_hand
			}
			boy = male
			girl = female
		}
		ep2_african_bow_01_drawn_left = {
			index = 78
			male = {
				1 = ep2_african_bow_01_drawn_left_hand
			}
			female = {
				1 = ep2_african_bow_01_drawn_left_hand		
			}
			boy = male
			girl = female
		}	
		ep2_african_bow_01_left = {
			index = 79
			male = {
				1 = ep2_african_bow_01_left_hand
			}
			female = {
				1 = ep2_african_bow_01_left_hand		
			}
			boy = male
			girl = female
		}	
		# Arrows
		ep2_arrow_right = {
			index = 84
			male = {
				1 = ep2_arrow_a_right_hand
				1 = ep2_arrow_b_right_hand
			}
			female = {
				1 = ep2_arrow_a_right_hand	
				1 = ep2_arrow_b_right_hand
			}
			boy = male
			girl = female
		}	
		ep2_arrowBlunt_right = {
			index = 82
			male = {
				1 = ep2_arrowBlunt_a_right_hand
				1 = ep2_arrowBlunt_b_right_hand	
			}
			female = {
				1 = ep2_arrowBlunt_a_right_hand
				1 = ep2_arrowBlunt_b_right_hand	
			}
			boy = male
			girl = female
		}	
		# Lance
		ep2_lance_01_a_right = {
			index = 83
			male = {
				1 = ep2_lance_01_a_right_hand
			}
			female = {
				1 = ep2_lance_01_a_right_hand
			}
			boy = male
			girl = female
		}	
		ep2_hunting_sword_right = {
			index = 85
			male = {
				1 = ep2_hunting_sword_01_right_hand
			}
			female = {
				1 = ep2_hunting_sword_01_right_hand
			}
			boy = male
			girl = female
		}
		ep2_boar_spear_left = {
			index = 86
			male = {
				1 = ep2_boar_spear_01_a_left_hand
				1 = ep2_boar_spear_01_b_left_hand
			}
			female = {
				1 = ep2_boar_spear_01_a_left_hand
				1 = ep2_boar_spear_01_b_left_hand
			}
			boy = male
			girl = female
		}
		ep2_boar_spear_right = {
			index = 87
			male = {
				1 = ep2_boar_spear_01_a_right_hand
				1 = ep2_boar_spear_01_b_right_hand
			}
			female = {
				1 = ep2_boar_spear_01_a_right_hand
				1 = ep2_boar_spear_01_b_right_hand
			}
			boy = male
			girl = female
		}
	}

	#####################################
	#									#
	# 		  	   LEGWEAR 		        #
	#									#
	#####################################

	legwear = {
		all_legwear = {
			index = 0
			male = {
				1 = male_legwear_secular_western_common_01
				1 = male_legwear_secular_western_war_01
				1 = male_legwear_secular_mena_war_nobility_01
				1 = male_legwear_secular_mena_nobility_01
				1 = male_legwear_secular_fp1_common_01
				1 = male_legwear_secular_ep1_jester_01
				1 = m_legwear_sec_ep2_western_era4_war_nob_01
				1 = m_legwear_sec_ep2_western_era4_war_nob_02
			}
			female = {
				1 = female_legwear_secular_western_common_01
				1 = female_legwear_secular_western_war_01
				1 = female_legwear_secular_mena_war_nobility_01
				1 = female_legwear_secular_mena_nobility_01
				1 = female_legwear_secular_fp1_common_01
				1 = female_legwear_secular_ep1_jester_01
				1 = f_legwear_sec_ep2_western_era4_war_nob_01
				1 = f_legwear_sec_ep2_western_era4_war_nob_02
			}
			boy = male
			girl = female
		}

		no_legwear = {
			index = 1
			male = {
				1 = empty
			}
			female = {
				1 = empty
			}
			boy = male
			girl = female
		}

		western_common_legwear = {
			index = 2
			male = {
				1 = male_legwear_secular_western_common_01
			}
			female = {
				1 = female_legwear_secular_western_common_01
			}
			boy = male
			girl = female
		}

		western_war_legwear = {
			index = 3
			male = {
				1 = male_legwear_secular_western_war_01
			}
			female = {
				1 = female_legwear_secular_western_war_01
			}
			boy = male
			girl = female
		}

		mena_war_legwear = {
			index = 4
			male = {
				1 = male_legwear_secular_mena_war_nobility_01
			}
			female = {
				1 = female_legwear_secular_mena_war_nobility_01
			}
			boy = male
			girl = female
		}

		mena_common_legwear = {
			index = 5
			male = {
				1 = male_legwear_secular_mena_common_01
			}
			female = {
				1 = female_legwear_secular_mena_common_01
			}
			boy = male
			girl = female
		}

		mena_nobility_legwear = {
			index = 6
			male = {
				1 = male_legwear_secular_mena_nobility_01
			}
			female = {
				1 = female_legwear_secular_mena_nobility_01
			}
			boy = male
			girl = female
		}

		fp1_common_legwear = {
			index = 7
			male = {
				1 = male_legwear_secular_fp1_common_01
			}
			female = {
				1 = female_legwear_secular_fp1_common_01
			}
			boy = male
			girl = female
		}
		ep1_jester_legwear = {
			index = 8
			male = {
				1 = male_legwear_secular_ep1_jester_01
			}
			female = {
				1 = female_legwear_secular_ep1_jester_01
			}
			boy = male
			girl = female
		}
		ep2_western_war_legwear = {
			index = 9
			male = {
				1 = m_legwear_sec_ep2_western_era4_war_nob_01
				1 = m_legwear_sec_ep2_western_era4_war_nob_02
			}
			female = {
				1 = f_legwear_sec_ep2_western_era4_war_nob_01
				1 = f_legwear_sec_ep2_western_era4_war_nob_02
			}
			boy = male
			girl = female
		}
	}

	special_legwear = {

		special_wooden_leg = {
			index = 0
			male = {
				1 = male_legwear_special_wooden_leg_01
			}
			female = {
				1 = female_legwear_special_wooden_leg_01
			}
			boy = male
			girl = female
		}
	}




	#####################################
	#									#
	# 		  	   CLOAKS 		        #
	#									#
	#####################################


	cloaks = {
		no_cloak = {
			index = 0
			male = {
				1 = empty
			}
			female = {
				1 = empty
			}
			boy = male 
			girl = female
		}

		western_royalty = {
			index = 1
			male = {
				1 = male_cloaks_secular_western_royalty_01
			}
			female = {
				1 = female_cloaks_secular_western_royalty_01
			}
		}

		fp1_cloak = {
			index = 2
			male = {
				1 = male_cloaks_secular_fp1_nobility_01
				1 = male_cloaks_secular_fp1_nobility_02
				1 = male_cloaks_secular_fp1_nobility_03
				1 = male_furs_secular_fp1_nobility_01
				1 = male_furs_secular_fp1_nobility_02
				1 = male_furs_secular_fp1_nobility_03
			}
			female = {
				1 = female_cloaks_secular_fp1_nobility_01
				1 = female_cloaks_secular_fp1_nobility_02
				1 = female_cloaks_secular_fp1_nobility_03
				1 = female_furs_secular_fp1_nobility_01
				1 = female_furs_secular_fp1_nobility_02
				1 = female_furs_secular_fp1_nobility_03
			}
		}

		fp1_cloak_berserker = {
			index = 3
			male = {
				1 = male_furs_secular_fp1_nobility_03
			}
			female = {
				1 = empty
			}
		}

		ep1_cloak_adventurer = {
			index = 4
			male = {
				1 = male_cloaks_secular_ep1_adventurer_01
			}
			female = {
				1 = empty
			}
		}

		ep2_cloak_western_travel = {
			index = 5
			male = {
				1 = m_cloaks_sec_ep2_western_travel_01
			}
			female = {
				1 = f_cloaks_sec_ep2_western_travel_01
			}
			boy = male 
			girl = female
		}

		ep2_cloak_mena_travel = {
			index = 6
			male = {
				1 = m_cloaks_sec_ep2_mena_travel_01
			}
			female = {
				1 = f_cloaks_sec_ep2_mena_travel_01
			}
			boy = male 
			girl = female
		}

		ep2_cloak_western_era1_low_nobility = {
			index = 7
			male = {
				1 = m_cloaks_sec_ep2_western_era1_nob_01_lo
			}
			female = {
				1 = empty
			}
		}

		ep2_cloak_western_era1_high_nobility = {
			index = 8
			male = {
				1 = m_cloaks_sec_ep2_western_era1_nob_01_hi
			}
			female = {
				1 = empty
			}
		}

		sp2_cloak_imperial = {
				index = 10
				male = {
					1 = m_cloaks_sec_sp2_western_imp_01
				}
				female = {
					1 = f_cloaks_sec_sp2_western_imp_01
				}
				boy = male
				girl = female
		}

		all_cloaks = {
			index = 9
			male = {
				1 = male_cloaks_secular_western_royalty_01
				1 = male_cloaks_secular_fp1_nobility_01
				1 = male_cloaks_secular_fp1_nobility_02
				1 = male_cloaks_secular_fp1_nobility_03
				1 = male_furs_secular_fp1_nobility_01
				1 = male_furs_secular_fp1_nobility_02
				1 = male_furs_secular_fp1_nobility_03
				1 = male_furs_secular_fp1_nobility_03
				1 = male_furs_secular_fp1_nobility_03
				1 = male_cloaks_secular_ep1_adventurer_01
				1 = m_cloaks_sec_ep2_western_travel_01
				1 = m_cloaks_sec_ep2_mena_travel_01
				1 =	m_cloaks_sec_ep2_western_era1_nob_01_low
				1 =	m_cloaks_sec_ep2_western_era1_nob_01_high
			}
			female = {
				1 = female_cloaks_secular_fp1_nobility_01
				1 = female_cloaks_secular_fp1_nobility_02
				1 = female_cloaks_secular_fp1_nobility_03
				1 = female_furs_secular_fp1_nobility_01
				1 = female_furs_secular_fp1_nobility_02
				1 = female_furs_secular_fp1_nobility_03
				1 = f_cloaks_sec_ep2_western_travel_01
				1 = f_cloaks_sec_ep2_mena_travel_01
			}
			
			boy = male 
			girl = female
		}
	}		

	#####################################
	#									#
	# 		   secondary prop		    #
	#									#
	#####################################

	props_2 = {
		ep1_mena_book_big = {
			index = 0
			male = {
				1 = ep1_mena_book_big_01
			}
			female = {
				1 = ep1_mena_book_big_01		
			}
			boy = male
			girl = female
		}
	}

#toto
	props_3 = {
		ep2_arrow_right = {
			index = 0
			male = {
				1 = ep2_arrow_a_right_hand
				1 = ep2_arrow_b_right_hand
			}
			female = {
				1 = ep2_arrow_a_right_hand
				1 = ep2_arrow_b_right_hand				
			}
			boy = male
			girl = female
		}
	}

	props_5 = {
		ep2_arrowBlunt_right = {
			index = 0
			male = {
				1 = ep2_arrowBlunt_a_right_hand
				1 = ep2_arrowBlunt_b_right_hand
			}
			female = {
				1 = ep2_arrowBlunt_a_right_hand
				1 = ep2_arrowBlunt_b_right_hand
			}
			boy = male
			girl = female
		}
	}	

	props_6 = {
		ep2_lance_01_right = {
			index = 0
			male = {
				1 = ep2_lance_01_a_right_hand
				1 = ep2_lance_01_b_right_hand
				1 = ep2_lance_01_c_right_hand
			}
			female = {
				1 = ep2_lance_01_a_right_hand
				1 = ep2_lance_01_b_right_hand
				1 = ep2_lance_01_c_right_hand
			}
			boy = male
			girl = female
		}
	}	

	props_7 = {
		ep2_westen_jousting_shield_01 = {
			index = 0
			male = {
				1 = ep2_westen_jousting_shield_01_a
			}
			female = {
				1 = ep2_westen_jousting_shield_01_a
			}
			boy = male
			girl = female
		}
	}
	
	
	
	#####################################
	#									#
	# 		   Animated props		    #
	#									#
	#####################################


	animated_props = {
		ep2_falcon_peregrine = {
			index = 0
			male = {
				1 = ep2_falcon_peregrine_01
			}
			
			female = {
				1 = ep2_falcon_peregrine_01
			}
			boy = male
			girl = female
		}

		ep2_horse_idle = {
			index = 1
			male = {
				1 = ep2_horse_01_idle
				1 = ep2_horse_01_alt_idle
				1 = ep2_horse_02_idle
				1 = ep2_horse_02_alt_idle
				1 = ep2_horse_03_idle
				1 = ep2_horse_03_alt_idle
			}
			female = {
				1 = ep2_horse_01_idle
				1 = ep2_horse_01_alt_idle
				1 = ep2_horse_02_idle
				1 = ep2_horse_02_alt_idle
				1 = ep2_horse_03_idle
				1 = ep2_horse_03_alt_idle
			}
			boy = male
			girl = female
		}
		
		ep2_horse_walk = {
			index = 2
			male = {
				1 = ep2_horse_01_walk
				1 = ep2_horse_01_alt_walk
				1 = ep2_horse_02_walk
				1 = ep2_horse_02_alt_walk
				1 = ep2_horse_03_walk
				1 = ep2_horse_03_alt_walk
			}
			female = {
				1 = ep2_horse_01_walk
				1 = ep2_horse_01_alt_walk
				1 = ep2_horse_02_walk
				1 = ep2_horse_02_alt_walk
				1 = ep2_horse_03_walk
				1 = ep2_horse_03_alt_walk
			}
			boy = male
			girl = female
		}

		ep2_horse_victory = {
			index = 3
			male = {
				1 = ep2_horse_01_victory
				1 = ep2_horse_01_alt_victory
				1 = ep2_horse_02_victory
				1 = ep2_horse_02_alt_victory
				1 = ep2_horse_03_victory
				1 = ep2_horse_03_alt_victory
			}
			female = {
				1 = ep2_horse_01_victory
				1 = ep2_horse_01_alt_victory
				1 = ep2_horse_02_victory
				1 = ep2_horse_02_alt_victory
				1 = ep2_horse_03_victory
				1 = ep2_horse_03_alt_victory
			}
			boy = male
			girl = female
		}

		ep2_horse_loss = {
			index = 4
			male = {
				1 = ep2_horse_01_loss
				1 = ep2_horse_01_alt_loss
				1 = ep2_horse_02_loss
				1 = ep2_horse_02_alt_loss
				1 = ep2_horse_03_loss
				1 = ep2_horse_03_alt_loss
			}
			female = {
				1 = ep2_horse_01_loss
				1 = ep2_horse_01_alt_loss
				1 = ep2_horse_02_loss
				1 = ep2_horse_02_alt_loss
				1 = ep2_horse_03_loss
				1 = ep2_horse_03_alt_loss
			}
			boy = male
			girl = female
		}

		ep2_horse_tiltedl = {
			index = 5
			male = {

				1 = ep2_horse_01_tiltedl
				1 = ep2_horse_01_alt_tiltedl
				1 = ep2_horse_02_tiltedl
				1 = ep2_horse_02_alt_tiltedl
				1 = ep2_horse_03_tiltedl
				1 = ep2_horse_03_alt_tiltedl
			}
			female = {
				1 = ep2_horse_01_tiltedl
				1 = ep2_horse_01_alt_tiltedl
				1 = ep2_horse_02_tiltedl
				1 = ep2_horse_02_alt_tiltedl
				1 = ep2_horse_03_tiltedl
				1 = ep2_horse_03_alt_tiltedl
			}
			boy = male
			girl = female
		}

		ep2_horse_cgallop = {
			index = 6
			male = {
				1 = ep2_horse_01_cgallop
				1 = ep2_horse_01_alt_cgallop
				1 = ep2_horse_02_cgallop
				1 = ep2_horse_02_alt_cgallop
				1 = ep2_horse_03_cgallop
				1 = ep2_horse_03_alt_cgallop
			}
			female = {
				1 = ep2_horse_01_cgallop
				1 = ep2_horse_01_alt_cgallop
				1 = ep2_horse_02_cgallop
				1 = ep2_horse_02_alt_cgallop
				1 = ep2_horse_03_cgallop
				1 = ep2_horse_03_alt_cgallop
			}
			boy = male
			girl = female
		}

		ep2_horse_cgallopl = {
			index = 7
			male = {
				1 = ep2_horse_01_cgallopl
				1 = ep2_horse_01_alt_cgallopl
				1 = ep2_horse_02_cgallopl
				1 = ep2_horse_02_alt_cgallopl
				1 = ep2_horse_03_cgallopl
				1 = ep2_horse_03_alt_cgallopl
			}
			female = {
				1 = ep2_horse_01_cgallopl
				1 = ep2_horse_01_alt_cgallopl
				1 = ep2_horse_02_cgallopl
				1 = ep2_horse_02_alt_cgallopl
				1 = ep2_horse_03_cgallopl
				1 = ep2_horse_03_alt_cgallopl
			}
			boy = male
			girl = female
		}
	}